#!/bin/bash

# Installation of necessary Python Packages necessary for the
# workshop "Daten analysieren und transformieren mit Python"

conda install -y -q jupyter

conda install -y -q pandas
conda install -y -q matplotlib

conda install -y -q mysql-connector-python
conda install -y -q sqlalchemy
conda install -y -q mysqlclient